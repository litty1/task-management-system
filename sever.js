const express = require("express");
const app = express();
const fileupload =require('express-fileupload');
const mongo=require('mongodb').MongoClient;
const mongoose =require('mongoose');
var uri = "mongodb://localhost:27017/task";
const multer=require('multer');


var bodyParser = require("body-parser");
app.use("/uploads", express.static("./uploads"));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false});
const connection = mongoose.connection;
connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
})
const Schema =mongoose.Schema
global.userid={};
const ObjectId = Schema.ObjectId;







const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (request, file, callback) {
    callback(null, Date.now() + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
});
  



const userSchema = new Schema({
    firstname:{

        type:String
    },
    surname:{

        type:String
    },
    email:{

        type:String
    },
   phone:{

        type:String
    },
    password:{

        type:String
    }
},{ timestamps:true})

const User = mongoose.model('User',userSchema)




const taskSchema = new Schema({
    tname:{

        type:String
    },
    status:{

        type:String
    },
    number:{

        type:String
    },
    date:{

        type:String
    },
    createdby:{

        type:ObjectId 
    }
   
},{ timestamps:true})

const Task = mongoose.model('Task',taskSchema)


const imgSchema = new Schema({
   img:{

        type:String
    },
    task_id:{

        type:ObjectId
    }
    
   
},{ timestamps:true})

const images = mongoose.model('images',imgSchema )





    
    app.post("/login",function(req,res){
        const email =req.body.email;
        const password =req.body.password;

        console.log(req.body);

        User.findOne({email: email,password:password},function(err,user){
            if(err){
                console.log(err);
               
                return res.status(500).send();
            }

            if(!user){
                console.log("unsucessfull"); 
                res.send("unsucessfull"); 
                return res.status(404).send();
                
            }
            else{
                userid=user;
                res.json(user);   
                
            }
            
     
        })

    });

    app.post("/register",function(req,res){
        console.log(req.body);
        const password =req.body.password;
        const firstname =req.body.firstname;
        const surname =req.body.surname;
        const email =req.body.email;
        const phone =req.body.phone;
        console.log(password);

        var newuser =new User();
        newuser.password = password;
        newuser.firstname = firstname;
        newuser.surname= surname;
        newuser.email=email;
        newuser.phone=phone;
        console.log(newuser)
        newuser.save(function(err,savedUser) {
            if (err) return console.error(err);
            console.log("savedUser")
            console.log(savedUser)
           console.log("Document inserted succussfully!");
          });res.send("insert sucessfuly") 
    });



    app.post("/Addtask",function(req,res){
        console.log(req.body);
        const date =req.body.date;
        const tname =req.body.tname;
        const status =req.body.status;
        const createdby=userid._id;
        const number =req.body.number;
        const taskImage=req.body.taskImage;
        console.log(status);

        var newtask =new Task();
        newtask.date = date;
        newtask.tname = tname;
        newtask.status= status;
        newtask.createdby=createdby;
        newtask.number=number;
        console.log(newtask)
        newtask.save(function(err,savedTask) {
            if (err) return console.error(err);
            console.log("savedTask")
            console.log(savedTask)
           console.log("Document inserted succussfully!");
          });res.send("insert sucessfuly") 
    });


    app.put("/update/:Taskid", function(req, res)  {


        Task.findByIdAndUpdate(req.params.Taskid, req.body, function (err, post) {
            if (err) return next(err);
              
            res.send(post)
          });
          });
    
    
       

          app.delete("/delete", function(req, res)  {


            Taskid = req.query.Taskid;
              Task.findByIdAndRemove(Taskid,function (err, post) {
                if (err) return next(err);
                
                res.send(post)
      
              });
                });
   
   
                app.get("/view/:id", function (req, res) {

                    Task.find({createdby:req.param.id}, function (err, task) {
                           console.log(task);     
                         res.json(task);
    
                         
                        });
      
                    });
    
                    app.get("/viewOne/:id", function (req, res) {
    
                        Task.findOne({createdby:req.param.id}, function (err, task) {
                               console.log(task);     
                             res.json(task);
        
                             
                            });
          
                        });
                 
                        
                        app.post("/add_image", upload.single("img"), async function (req, res) {

                            console.log(req.query.id);
                             var docm = {
      task_id: req.query.id,
      img: req.file.path + "" + req.file.filename,
    };

    await images.create(docm, function (err, post) {
      if (err) return 
      res.status(200);
      res.send(post);
    });
                            });


                            app.delete("/delete_image",  async function (req, res, next) {
                                if (req.query.id) {
                                  await images.findByIdAndRemove(req.query.id, function (err, post) {
                                    if (err) return next(err);
                                    res.status(200);
                                    res.json(post);
                                  });
                                } else {
                                  res.send("give query param with id");
                                }
                              });
                                                  
                            


    
 app.listen(3000);